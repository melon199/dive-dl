import torch
from torch import nn

torch.cuda.is_available()  # 输出 True
torch.cuda.device_count()  # 输出 1    查看GPU数量
torch.cuda.current_device()  # 输出 0  查看当前GPU索引号
torch.cuda.get_device_name(0)  # 根据索引号查看GPU名字

#%% Tensor的GPU计算
# 默认情况下，Tensor会被存在内存上
x = torch.tensor([1, 2, 3])
x
# 使用.cuda()可以将CPU上的Tensor转换（复制）到GPU上
x = x.cuda(0)
# 可以通过Tensor的device属性来查看该Tensor所在的设备
x.device

# 可以直接在创建的时候就指定设备
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
x = torch.tensor([1, 2, 3], device=device)
# or
x = torch.tensor([1, 2, 3]).to(device)
x
# 对在GPU上的数据进行运算，那么结果还是存放在GPU上
# 存储在不同位置中的数据是不可以直接进行计算的。即存放在CPU上的数据不可以直接与存放在GPU上的数据进行运算
#%% 模型的GPU计算
net = nn.Linear(3, 1)
list(net.parameters())[0].device
net.cuda()
list(net.parameters())[0].device
# 我么需要保证模型输入的Tensor和模型都在同一设备上
x = torch.rand(2,3).cuda()
net(x)


