import torch
from torch import nn

#%% 直接使用save函数和load函数分别存储和读取Tensor
# 存储Tensor变量
x = torch.ones(3)
torch.save(x, 'x.pt')
x2 = torch.load('x.pt')
x2
# 存储Tensor列表
y = torch.zeros(4)
torch.save([x, y], 'xy.pt')
xy_list = torch.load('xy.pt')
xy_list
# 存储并读取一个从字符串映射到Tensor的字典
torch.save({'x': x, 'y': y}, 'xy_dict.pt')
xy = torch.load('xy_dict.pt')
xy


#%% 读写模型
# state_dict是一个从参数名称隐射到参数Tesnor的字典对象
# 只有具有可学习参数的层(卷积层、线性层等)才有state_dict中的条目
class MLP(nn.Module):
    def __init__(self):
        super(MLP, self).__init__()
        self.hidden = nn.Linear(3, 2)
        self.act = nn.ReLU()
        self.output = nn.Linear(2, 1)

    def forward(self, x):
        a = self.act(self.hidden(x))
        return self.output(a)

net = MLP()
net.state_dict()

optimizer = torch.optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
optimizer.state_dict()

#%% 保存和加载模型
# 1.仅保存和加载模型参数(state_dict)
# 2.保存和加载整个模型
# 保存
torch.save(model.state_dict(), PATH) # 推荐的文件后缀名是pt或pth
# 加载
model = TheModelClass(*args, **kwargs)
model.load_state_dict(torch.load(PATH))
# 整个模型保存加载
torch.save(model, PATH)
model = torch.load(PATH)
#%% 实例化上面
X = torch.randn(2, 3)
Y = net(X)

PATH = "./net.pt"
torch.save(net.state_dict(), PATH)

net2 = MLP()  # !nvidia-smi model = TheModelClass(*args, **kwargs)
net2.load_state_dict(torch.load(PATH))
Y2 = net2(X)
Y2 == Y
# 因为这net和net2都有同样的模型参数，那么对同一个输入X的计算结果将会是一样的
