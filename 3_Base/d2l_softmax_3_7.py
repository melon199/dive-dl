#%% 导入所需的包或模块
import torch
from torch import nn
from torch.nn import init
import d2lzh_pytorch as d2l

#%% 获取和读取数据
batch_size = 256
train_iter, test_iter = d2l.load_data_fashion_mnist(batch_size)
# train_iter, test_iter = d2l.load_data_mnist(batch_size)
#%% 定义和初始化模型
num_inputs = 784
num_outputs = 10


class LinearNet(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(LinearNet, self).__init__()
        self.linear = nn.Linear(num_inputs, num_outputs)

    def forward(self, x): # x shape: (batch, 1, 28, 28)
        y = self.linear(x.view(x.shape[0], -1))  # 展开成一行
        return y


net = LinearNet(num_inputs, num_outputs)  # 实例化
init.normal_(net.linear.weight, mean=0, std=0.01)
init.constant_(net.linear.bias, val=0)
#%% softmax和交叉熵损失函数
loss = nn.CrossEntropyLoss()
#%% 定义优化算法
optimizer = torch.optim.SGD(net.parameters(), lr=0.01)
#%% 训练模型
num_epochs = 50
d2l.train_ch3(net, train_iter, test_iter, loss, num_epochs, batch_size, None, None, optimizer)

