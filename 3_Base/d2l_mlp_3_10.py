#%%
import torch
from torch import nn
from torch.nn import init
import numpy as np
import d2lzh_pytorch as d2l
#%% 定义模型
# num_inputs, num_outputs, num_hidden_1, num_hidden_2, num_hidden_3 = 784, 10, 512, 256, 64
num_inputs, num_outputs, num_hidden_1 = 784, 10, 256
net = nn.Sequential(
        d2l.FlattenLayer(),  # 每个样例展成一行 784
        nn.Linear(num_inputs, num_hidden_1),
        nn.ReLU(),
        # nn.Linear(num_hidden_1, num_hidden_2),
        # nn.ReLU(),
        # nn.Linear(num_hidden_2, num_hidden_3),
        # nn.ReLU(),
        nn.Linear(num_hidden_1, num_outputs),
        )

for params in net.parameters():
    init.normal_(params, mean=0, std=0.01)

#%% 读取数据并训练模型
batch_size = 256
train_iter, test_iter = d2l.load_data_fashion_mnist(batch_size)
# train_iter, test_iter = d2l.load_data_mnist(batch_size)
loss = torch.nn.CrossEntropyLoss()

optimizer = torch.optim.SGD(net.parameters(), lr=0.01)

num_epochs = 50
d2l.train

_ch3(net, train_iter, test_iter, loss, num_epochs, batch_size, None, None, optimizer)



