#%% 模块，数据导入 必须运行
import sys
import torchvision
from torch.utils.data import DataLoader
import d2lzh_pytorch as d2l
# 数据下载
data_train = torchvision.datasets.FashionMNIST(
    "./data", train=True, transform=torchvision.transforms.ToTensor(), download=True)
data_test = torchvision.datasets.FashionMNIST(
    "./data", train=False, transform=torchvision.transforms.ToTensor(), download=True)
#%% 查看一下数据类型，长度，图长什么样 可以不运行
print(type(data_train))
print(len(data_train), len(data_test))  # 60000 10000
feature, label = data_train[0]
print(feature.shape, label)  # Channel x Height x Width
X, y = [], []
for i in range(10):
    X.append(data_train[i][0])
    y.append(data_train[i][1])
d2l.show_fashion_mnist(X, d2l.get_fashion_mnist_labels(y))
#%% 对数据进行打包  集成为函数d2l.load_data_fashion_mnist(batch_size)
if sys.platform.startswith('win'):
    num_workers = 0  # 0表示不用额外的进程来加速读取数据
else:
    num_workers = 4
batch_size = 256
train_iter = DataLoader(data_train, batch_size=batch_size, shuffle=True, num_workers=num_workers)
test_iter = DataLoader(data_test, batch_size=batch_size, shuffle=False, num_workers=num_workers)
#%% 查看读取一遍数据所需时间
import time
start = time.time()
for X, y in train_iter:
    continue
print('%.2f sec' % (time.time() - start))
